{ config, pkgs, ... }:
{

  programs.zsh.enable = true;
  users.defaultUserShell = "/run/current-system/sw/bin/zsh";


  programs.zsh.interactiveShellInit = ''
    export ZDOTDIR=${pkgs.zsh-prezto}/
    export NIXOS=/etc/nixos/
    export ZSHCONFIG=$NIXOS/dotfiles/zsh
    source $ZSHCONFIG/zpreztorc
    source "$ZDOTDIR/init.zsh"
    source $ZSHCONFIG/zsh_functions
    PATH="$PATH:$HOME/.cargo/bin"
    PATH="$PATH:$HOME/.local/bin"
    # NPM can't install to the default location in /nix/store because of its immutability,
    # so create ~/.npm-packages/ and a ~/.npmrc file with "prefix=$HOME/.npm-packages" in it.
    NPM_PACKAGES="$HOME/.npm-packages"
    PATH="$NPM_PACKAGES/bin:$PATH"
    # If I want to install stuff not available via the nixos pkg store, I install it to $HOME/.bin
    HISTFILE=~/.zsh_history
  '';

  programs.zsh.promptInit = ''
     autoload -U promptinit && promptinit && prompt sorin 
  '';

  programs.zsh.shellAliases = {
    la="ls -A";
    lla="ll -A";
    lr="ls -R";
    lx="ll -BX";
    lz="ll -rS";
    no="ls";
    lj="ls *.java";

    fastping="ping -c 100 -i .2";
    ducks="du -cksh * | sort -rn | head";

    busy="cat /dev/urandom | hexdump -C | grep 'ca fe'"; 

    update="$(nix-build --no-out-link update.nix)/bin/update";

    ga="git add";
    gp="git push";
    gl="git pull";
    gd="git diff";
    gst="git status";
    gss="git status -s";
    glog="git log --oneline --decorate --graph";
    grh="git reset HEAD";
    glum="git pull upstream master";
    gwch="git whatchanged -p --abbrev-commit --pretty=medium";
    gcurr="git rev-parse --abbrev-ref HEAD";
    
    currDate="date +%Y-%m-%d";

  };

}
