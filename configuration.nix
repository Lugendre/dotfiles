# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, rsh, ... }:

let
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./zsh.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelParams = [ "acpi_osi=\"!Windows 2015\"" ];

  networking.hostName = "lugendre-pc"; # Define your hostname.
  networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Fonts
  fonts.enableDefaultFonts = true;
  fonts.enableFontDir = true;
  fonts.enableGhostscriptFonts = true;
  fonts.fontconfig.enable = true;
  fonts.fonts = with pkgs;
    [  source-code-pro
       noto-fonts-cjk
       powerline-fonts
    ];

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp0s20f3.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
 
  # bluetooth Settings
  hardware.bluetooth.enable = true;

  nix.trustedUsers = [ "root" "lugendre" ];
 
  i18n.consoleFont = "Lat2-Terminus16";
  i18n.consoleKeyMap = "jp106";

  i18n = {
    defaultLocale = "ja_JP.UTF-8";
    inputMethod.enabled = "ibus";
    inputMethod.ibus.engines = with pkgs.ibus-engines;
      [ anthy mozc ];
  };

  # Set your time zone.
  time.timeZone = "Asia/Tokyo";

  programs.bash.enableCompletion = true;

  environment.interactiveShellInit = ''
    export PATH="$PATH:$HOME/.local/bin";
  '';

  environment.variables = {
    EDITOR="nvim";
    BROWSER="google-chrome-stable";
    FILEMANAGER="nautilus";
  };
  
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget curl git gcc gnumake binutils pkg-config zip unzip xclip htop
    neovim openal powershell byobu gnome3.gnome-terminal xorg.xev xorg.xprop
    vscode google-chrome evince texlive.combined.scheme-full wpa_supplicant_gui
    nox traceroute usbutils coreutils unrar p7zip tree tmux screen
    python2 python3 ruby cmake clang nodejs gnuplot geeqie pciutils
    transmission-gtk paraview gmsh vlc mplayer mpv
    rustup docker zsh-prezto openmpi git-lfs zlib cachix
    # for security (Yubikey & GPG)
    yubikey-manager yubikey-personalization-gui opensc gnupg
    # for rsh
    inetutils
    rofi nitrogen xmobar trayer volumeicon gnome3.nautilus gnome3.gnome-screenshot
    (all-hies.selection { selector = p : { inherit (p) ghc865; };})
    # workaround
    # for html5 player
    ffmpeg-full
    # for mozc_tool
    qt5.full
  ];

  # Yubikey Settings
  services.udev.packages = [ pkgs.yubikey-personalization ];

  # Docker Settings
  virtualisation.docker.enable = false;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "jp";
  # services.xserver.xkbOptions = "eurosign:e";
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.optimus_prime.enable = true;
  hardware.nvidia.optimus_prime.nvidiaBusId = "PCI:01:00:00";
  hardware.nvidia.optimus_prime.intelBusId = "PCI:00:02:00";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Enable the Xmonad Environment.
  services.xserver.windowManager.xmonad.enable = true;
  services.xserver.windowManager.xmonad.enableContribAndExtras = true;
  services.xserver.windowManager.default = "xmonad";
  services.xserver.desktopManager.default = "none";
  services.xserver.desktopManager.xterm.enable = false;
  services.xserver.windowManager.xmonad.config = ''
    import XMonad
    import XMonad.Util.Run 
    import XMonad.Hooks.DynamicLog
    
    main = do
      xmproc <- spawnPipe "xmobar"
      xmonad $ defaultConfig
        { terminal = "gnome-terminal"
        , logHook = myLogHook xmproc
	, modMask = mod4Mask
        }
    
    myLogHook dest = dynamicLogWithPP defaultPP { ppOutput = hPutStrLn dest
                                                , ppVisible = wrap "(" ")"
						}
    '';

  # Enable the LightDM Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.displayManager.sessionCommands = ''
    # nitrogen --set-scaled /home/lugendre/
    xrandr --setprovideroutputsource modesetting NVIDIA-0
    xrandr --auto
    trayer --edge top --align center --SetDockType true --setPartialStrut false --expand true --width 10 --transparent true --tint 0x000000 --height 14 &
    ibus-daemon -drx
    rofi -show run -modi run -location 1 -width 100 \
     		 -lines 2 -line-margin 0 -line-padding 1 \
     		 -separator-style none -font "mono 10" -columns 9 -bw 0 \
     		 -disable-history \
     		 -hide-scrollbar \
     		 -color-window "#222222, #222222, #b1b4b3" \
     		 -color-normal "#222222, #b1b4b3, #222222, #005577, #b1b4b3" \
    		 -color-active "#222222, #b1b4b3, #222222, #007763, #b1b4b3" \
    		 -color-urgent "#222222, #b1b4b3, #222222, #77003d, #b1b4b3" \
    		 -kb-row-select "Tab" -kb-row-tab ""
    xmobar /etc/nixos/dotfiles/xmobarrc &
    nautilus &
    gnome-terminal &
    google-chrome-stable &
  '';

  services.compton = {
    enable = true;
    fade = true;
    activeOpacity = "0.7";
    inactiveOpacity = "0.5";
    shadow = true;
    fadeDelta = 4;
  };

  programs.tmux.enable = true;

  # For yubikey piv
  services.pcscd.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.lugendre = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" "audio" ]; # Enable ‘sudo’ for the user.
    createHome = true;
    uid = 1000;
    shell = pkgs.zsh;
  };

  users.extraUsers.root.extraGroups = [ "audio" ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}

